<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Auth {
    public function cek_auth()
	{
		$this->cend =& get_instance();
		$this->sesi  = $this->cend->session->userdata('isLogin');
		$this->hak = $this->cend->session->userdata('stat');
		if($this->sesi != TRUE){
			redirect('login','refresh');
			exit();
		}
		
	}
	public function hak_akses($kecuali="")
	{	
    	if($this->hak==$kecuali){ 
    		echo "<script>alert('Anda tidak berhak mengakses halaman ini!');</script>";
    		redirect('admin/admin');
    	}elseif ($this->hak=="") {
    		echo "<script>alert('Anda belum login!');</script>";
    		redirect('login');
    	}else{
 
    	}
	}
}
 