<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
               
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">

            <div class="box-header with-border">

              <a href=" <?php echo   site_url('testimonial/createTestimonial');?>"  ><h3 class="box-title">
              <td><button type="submit" class="btn btn-block btn-primary"> <span class="glyphicon glyphicon-plus"></span>
              Create Testimonial</button></td></a></div>
                              
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                      <th>ID</th>
                      <th>TITLE</th>
                      <th>CONTENT</th>
                      <th>AUTHOR</th>
                      <th>SRC</th>
                      <th colspan="2">ACTION</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $values) { ?>
                    <tr>
                    <td><?php echo $values['ID'];?>      </td>
                    <td><?php echo $values['TITLE'];?>   </td>
                    <td><?php echo $values['CONTENT'];?> </td>
                    <td><?php echo $values['AUTHOR'];?> </td>
                    <td><?php echo $values['SRC']; ?>    </td>
                    <td><a href="   <?php echo   site_url  ('testimonial/updateTestimonial/'.$values['ID'] );?>">
                      <button type="button" class="btn btn-block btn-primary btn-xs">Update</button></a>
                    </td>
                    <td><a onClick="return warning('Are You Sure to Delete This Data?');" href="   <?php echo   site_url  ('testimonial/deleteTestimonial/'.$values['ID']);?>">
                      <button type="button" class="btn btn-block btn-primary btn-xs">Delete</button></a>
                    </td>
                    </tr>
                <?php } ?>
               
                 </tbody>
                 <tfoot>
                  <tr>
                  <th>ID</th>
                  <th>TITLE</th>
                  <th>CONTENT</th>
                  <th>AUTHOR</th>
                  <th>SRC</th>
                  <th colspan="2">ACTION</th>
                </tfoot>
              </table>

                </div>
              </div>
            </div>
          </div>
        </section>
      </div>




<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

