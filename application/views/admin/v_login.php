<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="Neon Admin Panel" />
  <meta name="author" content="" />

  <title>Kerajinan Batok | Login</title>

  <link rel="stylesheet" href="<?= base_url() ?>assets2/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets2/css/font-icons/entypo/css/entypo.css">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
  <link rel="stylesheet" href="<?= base_url() ?>assets2/css/bootstrap.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets2/css/neon-core.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets2/css/neon-theme.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets2/css/neon-forms.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets2/css/custom.css">

  <script src="<?= base_url() ?>assets2/js/jquery-1.11.0.min.js"></script>
  <script>$.noConflict();</script>
</head>

<body class="page-body login-page login-form-fall" data-url="http://neon.dev">

  <div class="login-container">
    <div class="login-header login-caret">  
      <a href="<?= site_url(); ?>admin/admin" class="logo">
        <center><img src="<?= base_url() ?>assets2/images/logo-2.png" width="500" alt="" /></center>
      </a>
      <center><p class="description">Halo user, silahkan login untuk mengakses area admin!</p></center>

      <!-- progress bar indicator -->
      <div class="login-progressbar-indicator">
        <h3>43%</h3>
        <span>sedang login...</span>
      </div>      
    </div>

    <div class="login-progressbar">
      <div></div>
    </div>    

    <div class="login-form"> 
      <div class="login-content">
        <?php echo validation_errors(); ?>
        <!-- <form method="post" role="form" id="form_login"> -->
        <?php echo form_open('admin/verifylogin', 'role="form" id="form_login"'); ?>
          <div class="form-group">   
            <div class="input-group">
              <div class="input-group-addon">
                <i class="entypo-mail"></i>
              </div>       
              <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-mask="email" autocomplete="on" />
            </div>  
          </div>
          
          <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">
                <i class="entypo-key"></i>
              </div>
              <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
            </div>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block btn-login">
              <i class="entypo-login"></i>
              Login
            </button>
          </div>         
        <?php echo form_close(); ?><!-- </form> -->
        
        <div class="login-bottom-links">
          <a href="extra-forgot-password.html" class="link">Lupa kata sandi?</a>
          <br />
          <a href="#">KerajinanBatok[dot]com</a>  - <a href="#">Kebijakan Privasi</a>
        </div> 
      </div>
    </div>  
  </div>


  <!-- Bottom scripts (common) -->
  <script src="<?= base_url() ?>assets2/js/gsap/main-gsap.js"></script>
  <script src="<?= base_url() ?>assets2/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
  <script src="<?= base_url() ?>assets2/js/bootstrap.js"></script>
  <script src="<?= base_url() ?>assets2/js/joinable.js"></script>
  <script src="<?= base_url() ?>assets2/js/resizeable.js"></script>
  <script src="<?= base_url() ?>assets2/js/neon-api.js"></script>
  <script src="<?= base_url() ?>assets2/js/jquery.validate.min.js"></script>
  <script src="<?= base_url() ?>assets2/js/neon-login.js"></script>


  <!-- JavaScripts initializations and stuff -->
  <script src="<?= base_url() ?>assets2/js/neon-custom.js"></script>


  <!-- Demo Settings -->
  <script src="<?= base_url() ?>assets2/js/neon-demo.js"></script>

</body>
</html>