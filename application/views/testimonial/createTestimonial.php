<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="info-warning">
  <?php
    $info = $this->session->flashdata('infoWarning');
    if (!empty($info)) {
    echo $info;
    }
  ?>
</div>       
<style>
.updatebox{margin: auto 90px auto ; }
</style>

<div class="box box-warning updatebox">
  <div class="box-header with-border">
    <h3 class="box-title">Create Testimonial</h3>
  </div>
            <!-- /.box-header -->
<div class="box-body">
 <form role="form" method="POST" action="<?php echo site_url('testimonial/prosesCreateTestimonial');?>">
  <div class="form-group">
    <label>Title</label>
      <select class="form-control" name="id_movie" required>
        <?php echo '<option>- Pilih Movie -</option>';
              foreach ($movie as $row) { echo ' <option value="'.$row['id'].'" >'.$row['title'].'</option>';}?>
     </select>
 </div>

    <div class="form-group">
      <label>Content</label>
        <textarea class="form-control" rows="3" placeholder="Enter ..." required name="content"></textarea>
    </div>

    <div class="form-group">
      <label>Author</label>
    <input type="text" class="form-control" placeholder="Enter ..." required name="author" >
    </div>
              
    <div class="form-group">
      <label>Status</label>
    <input type="text" class="form-control" placeholder="Enter ..." required name="status" >
    </div>
                   <!-- textarea -->
    <div class="form-group">
      <label>Src</label>
        <textarea class="form-control" rows="3" placeholder="Enter ..." required name="src" ></textarea>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <div class="btn-group">
          <button type="submit" class="btn btn-primary btn-flat" >Submit</button>
          <button type="reset" class="btn btn-warning btn-flat" >Reset</button>
          <a href="<?php echo base_url();?>testimonial" class="btn btn-default btn-flat">Cancel</a>
        </div>
      </div>
    </div>
  </form>
</div>
</div>
