<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="info-warning">
  <?php
  $info = $this->session->flashdata('infoWarning');
    if (!empty($info)) {
    echo $info;
    }
  ?>
</div>
 
<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo lang('groups_edit_group'); ?></h3>
          </div>
            <div class="box-body">
            <div class="box box-warning">
              <div class="box-header with-border">
              <h3 class="box-title">Update Testimonial</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <form role="form" method="POST" action="<?php echo site_url('testimonial/prosesUpdateTestimonial');?>" >
                <!-- text input -->
                <input type="hidden" name="id_movie" value="<?php echo $content->id_movie;?>">
                <input type="hidden" name="id" value="<?php echo $content->id;?>">
            
                <div class="form-group">
                  <label>Author</label>
                  <input type="text" required class="form-control" placeholder="Enter ..." name="author" value="<?php echo $content->author;?>">
                </div>
   
                <div class="form-group">
                  <label>Source</label>
                  <textarea class="form-control" required rows="3" name="src" placeholder="Enter ..."><?php echo $content->src;?></textarea>
                </div>

                <div class="form-group">
                  <label>Content</label>
                  <textarea class="form-control" required rows="3" name="content" placeholder="Enter ..." ><?php echo $content->content;?></textarea>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="btn-group"><button type="submit" class="btn btn-primary btn-flat" >Submit</button>
                    <button type="reset" class="btn btn-warning btn-flat" >Reset</button>
                    <a href="<?php echo base_url();?>testimonial" class="btn btn-default btn-flat">Cancel</a>
                   </div>
                  </div>
                </div>
            </form>
            </div>
            <!-- /.box-body -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

