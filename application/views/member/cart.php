 <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/css.css">
 <div class="kotakkeranjang">
 <h1 style="color:white">Shopping cart anda</h1>
 <?php echo form_open('cart/update'); ?>

 <table cellpadding="6" cellspacing="1" style="width:90%" border="0">

<tr>

 <th style="color:white">QTY</th>
 <th style="color:white">Item Description</th>
  <th style="color:white">Item Price</th>
  <th style="color:white">Sub-Total</th>
 </tr>

 <?php $i = 1; ?>

 <?php foreach($this->cart->contents() as $items): ?>

    <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>

    <tr>
    <td><?php echo form_input(array('name' => $i.'[qty]', 'value' =>
         $items['qty'], 'maxlength' => '3', 'size' => '5')); ?></td>
      <td>
        <?php echo $items['name']; ?>

       <?php if ($this->cart->has_options($items['rowid']) == TRUE):
?>

               <p>

   <?php foreach ($this->cart->product_options($items['rowid']) as
             $option_name => $option_value): ?>

         <strong><?php echo $option_name; ?>:</strong>
        <?php echo $option_value; ?><br />

       <?php endforeach; ?>
         </p>

        <?php endif; ?>

      </td>
      <td style="text-align:right">
     <?php echo $this->cart->format_number($items['price']); ?></td>
      <td style="text-align:right">$
  <?php echo $this->cart->format_number($items['subtotal']); ?></td>
    </tr>

 <?php $i++; ?>

 <?php endforeach; ?>

<tr>
  <td colspan="2"> </td>
  <td class="right"><strong>Total</strong></td>
  <td class="right">$
   <?php echo $this->cart->format_number($this->cart->total());
?></td>
 </tr>

 </table>
 

 <?php
             echo form_submit('submit','Update Your Cart ','id="submit"');
		     echo form_close();
                    ?>
  <p>  <?php echo '<a href="'.base_url().'index.php/user/member/index">Kembali</a>'?></p>
  </div>
  
  
  
  
  
  
  