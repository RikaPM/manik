 <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/css.css">
<div class="kotakbarang">
<h1>Daftar barang </h1>

 <?php if (!empty($product_list)): ?>
	<table cellpadding="6" cellspacing="1" style="width:70%" border="0">
	<tr>
	<th style="color:white">Nama Barang</th>
	<th style="color:white">Harga</th>
	<th style="color:white">Pilihan</th>
	</tr>
    <?php foreach($product_list as $product): ?>
	<tr>
    <th><?php echo $product->nama_barang ?></th> 
	<th>($ <?php echo $product->harga ?>)  </th>
    <th>
	<a href='<?php echo site_url("cart/add/$product->id_barang") ?>'>Beli</a> - <a href='<?php echo site_url("user/member/add_wish/$product->id_barang") ?>'>Wishlist</a>
   </th>
   </tr>
    <?php endforeach ?>
	</table>
 <?php else : ?>
 <p>Produk kosong.</p>
 <?php endif ?>
 </div>