<!DOCTYPE html>
<html>
<head>
  <title>Modern Home</title>
  <link rel="stylesheet" href="<?=base_url();?>assets/css/css.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/960_12_col.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.min.css">
  <script type="text/javascript" src="<?=base_url();?>assets/js/jquery.min.js">
  </script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/bootstrap.min.js">
  </script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/scripts.js">
  </script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script> 
  $(document).ready(function(){
    $("#user").click(function(){
      $("#panel").slideToggle("slow");
    });
  });
  </script>
</head>
<body style="background-image: url(<?=base_url();?>assets/img/WoodWallpaper_1920.jpg);"class="body">

    <div class="container_12 c1">
      <div class="grid_12 header">
        <div class="grid_1">
           <a href="<?= base_url(); ?>index.php/user/member/index"><img id="logo" src="<?=base_url();?>assets/img/logo.jpg"></a>
        </div>
        <div class="grid_2">
          <img id="user" src="<?=base_url();?>assets/img/chat.png">
          <h6 id="nama-user">Hello <?php echo $username; ?></h6>
          <div id="panel">
            <center><img src="<?=base_url();?>assets/img/user.png" class="user2"></center>
            <p id="name-user">User - Member since 2014</p>
            <a class="fsf" href="">Followers</a>
            <a class="fsf" href="">Sales</a>
            <a class="fsf2" href="">Friends</a>
            <br><br><br>
            <a id="link-profile" href="">Profile</a>
            <a id="link-sign-out" href="">Sign out</a>
          </div>
        </div>
        <div class="grid_3">
          <input type="text" class="search" value placeholder="seacrh here">
          <a href="">
            <img width="20px" height="20px" src="<?=base_url();?>assets/img/sc.png">
          </a>
        </div>
        <div class-="grid_6">
          <a class="nav" href="">
            <img id="cart" src="<?=base_url();?>assets/img/troli.png">
            <p id="jumlah">0</p>
          </a>
          <a class="nav" href="<?= base_url(); ?>index.php/user/c_user/logout" >Logout</a>
          <a class="nav" href="<?= base_url(); ?>index.php/user/member/index2">Wishlist</a>
          <a class="nav" href="">Costumer Service</a>
        </div>
      </div>
    </div>

    <div class="container_12 c2">
      <div class="grid_12 slide-home">
            <div class="carousel slide" id="carousel-gambar">
              <div class="carousel-inner">
                <div class="item active">
                    <img alt="" src="<?=base_url();?>assets/perabot/kursi+banana.jpg" style="height: 350px; width: 1000px" /><br>
                </div>
                <div class="item">
                    <img alt="" src="<?=base_url();?>assets/perabot/b_101975340.jpg" style="height: 350px; width: 1000px" /><br>
                </div>
                <div class="item">
                    <img alt="" src="<?=base_url();?>assets/perabot/Furniture-design.jpg" style="height: 350px; width: 1000px" /><br>
                </div>
                <div class="item">
                    <img alt="" src="<?=base_url();?>assets/perabot/furniture-designer.jpg" style="height: 350px; width: 1000px" /><br>
                </div>
                <div class="item">
                    <img alt="" src="<?=base_url();?>assets/perabot/at-bklyn-designs-2009-w-d-furniture-design-large2.jpg" style="height: 350px; width: 1000px" /><br>
                </div>
                <div class="item">
                    <img alt="" src="<?=base_url();?>assets/perabot/colorful-chair-furniture-design.jpg" style="height: 350px; width: 1000px" /><br>
                </div>
              </div> 
                  <a class="left carousel-control" href="#carousel-gambar" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                  <a class="right carousel-control" href="#carousel-gambar" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
    </div>
	
	
