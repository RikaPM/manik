<!DOCTYPE html>
<html>
<head>
  <title>Modern Home</title>
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/960_12_col.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.min.css">
  <script type="text/javascript" src="<?=base_url();?>assets/js/jquery.min.js">
  </script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/bootstrap.min.js">
  </script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/scripts.js">
  </script>
</head>
<body style="background-image: url(<?=base_url();?>assets/img/bg-wood.png);"class="body">

    <div class="container_12 c1">
      <div class="grid_12 header">
        <div class="grid_2">
           <a href=""><img id="logo" src="<?=base_url();?>assets/img/logo.jpg"></a>
        </div>
        <div class="grid_4">
          <input type="text" class="search" value placeholder="seacrh here">
          <a href="">
            <img width="20px" height="20px" src="<?=base_url();?>assets/img/sc.png">
          </a>
        </div>
        <div class-="grid_6">
          <a class="nav" href="">
            <img id="cart" src="<?=base_url();?>assets/img/troli.png">
            <p id="jumlah">0</p>
          </a>
         <a  class="nav" href="<?= base_url(); ?>index.php/login/login" >Login</a>
          <a class="nav" href="">Wishlist</a>
          <a class="nav" href="">Costumer Service</a>
        </div>
      </div>
    </div>

  <li>

    <div class="container_12 c2">
      <div class="grid_12 slide-home">
            <div class="carousel slide" id="carousel-gambar">
              <div class="carousel-inner">
                <div class="item active">
                    <img alt="" src="<?=base_url();?>assets/perabot/kursi+banana.jpg" style="height: 350px; width: 1000px" /><br>
                </div>
                <div class="item">
                    <img alt="" src="<?=base_url();?>assets/perabot/b_101975340.jpg" style="height: 350px; width: 1000px" /><br>
                </div>
                <div class="item">
                    <img alt="" src="<?=base_url();?>assets/perabot/Furniture-design.jpg" style="height: 350px; width: 1000px" /><br>
                </div>
                <div class="item">
                    <img alt="" src="<?=base_url();?>assets/perabot/furniture-designer.jpg" style="height: 350px; width: 1000px" /><br>
                </div>
                <div class="item">
                    <img alt="" src="<?=base_url();?>assets/perabot/at-bklyn-designs-2009-w-d-furniture-design-large2.jpg" style="height: 350px; width: 1000px" /><br>
                </div>
                <div class="item">
                    <img alt="" src="<?=base_url();?>assets/perabot/colorful-chair-furniture-design.jpg" style="height: 350px; width: 1000px" /><br>
                </div>
              </div> 
                  <a class="left carousel-control" href="#carousel-gambar" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                  <a class="right carousel-control" href="#carousel-gambar" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
    </div>

    <div class="container_12 c3">
      <div class="grid_9 navigation-content">
        <a class="nav-con" href="">
          <img id="sc-home" src="<?=base_url();?>assets/img/home.png">
        </a>
        <a class="nav-con" href="">Living Room</a>
        <a class="nav-con" href="">Dining Room</a>
        <a class="nav-con" href="">Bed Room</a>
        <a class="nav-con" href="">Kitchen</a>
      </div>
        <div class="grid_3 right-gambar">
          <p id="title-best-sell">Best Seller Item</p>
          <center>
          <a href=""><img class="best-sell" src="<?=base_url();?>assets/perabot/elegant-home-furniture-designs.jpg"></a>
          <table cellpadding="7">
              <tr>
                <td width="115px">Sleep back Sofa</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 900,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          <a href=""><img class="best-sell" src="<?=base_url();?>assets/perabot/Faux+Leather+Barrel+Chair.jpg"></a>
          <table cellpadding="7">
              <tr>
                <td width="115px">Barrel chair</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 500,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          <a href=""><img class="best-sell" src="<?=base_url();?>assets/perabot/stacking-outdoor-furniture-design.jpg"></a>
          <table cellpadding="7">
              <tr>
                <td width="115px">Stacking Outdoor</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 1,300,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          <a href=""><img class="best-sell" src="<?=base_url();?>assets/perabot/unique-chair_1.jpg"></a>
          <table cellpadding="7">
              <tr>
                <td width="115px">Chair Book</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 900,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          <a href=""><img class="best-sell" src="<?=base_url();?>assets/perabot/Summit+Mountain+Audio+Rack.jpg"></a>
          <table cellpadding="7">
              <tr>
                <td width="115px">Audio Rack</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 800,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </center>
        </div>
          <div class="grid_3 out-gambar">
            <a href=""><img class="gambar" src="<?=base_url();?>assets/perabot/264804.jpg"></a>
            <table cellpadding="7">
              <tr>
                <td width="115px">Classic Bed</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 1,000,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </div>
          <div class="grid_3 out-gambar">
            <a href=""><img class="gambar" src="<?=base_url();?>assets/perabot/Aldo+Dining+Table.jpg"></a>
            <table cellpadding="7">
              <tr>
                <td width="115px">Aldo Table</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 500,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </div>
          <div class="grid_3 out-gambar">
            <a href=""><img class="gambar" src="<?=base_url();?>assets/perabot/Carly+Sofa.jpg"></a>
            <table cellpadding="7">
              <tr>
                <td width="115px">Carly Sofa</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 1,200,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </div>
          <div class="grid_3 out-gambar">
            <a href=""><img class="gambar" src="<?=base_url();?>assets/perabot/Alex+Media+Tower.jpg"></a>
            <table cellpadding="7">
              <tr>
                <td width="115px">Media Tower</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 600,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </div>
          <div class="grid_3 out-gambar">
            <a href=""><img class="gambar" src="<?=base_url();?>assets/perabot/Coastal+Living+Resort+Seascape+Dining+Table.jpg"></a>
            <table cellpadding="7">
              <tr>
                <td width="115px">Seascape Table</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 400,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </div>
          <div class="grid_3 out-gambar">
            <a href=""><img class="gambar" src="<?=base_url();?>assets/perabot/contemporary-home-interior-furniture-design-orsay-collection-by-grange-bookcase.jpg"></a>
            <table cellpadding="7">
              <tr>
                <td width="115px">Orsay Bookcase</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 900,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </div>
          <div class="grid_3 out-gambar">
            <a href=""><img class="gambar" src="<?=base_url();?>assets/perabot/Galaxy+Audio+Rack.jpg"></a>
            <table cellpadding="7">
              <tr>
                <td width="115px">Galaxy Rack</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 900,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </div>
          <div class="grid_3 out-gambar">
            <a href=""><img class="gambar" src="<?=base_url();?>assets/perabot/Mission+Style+Expanding+Dining+Table.jpg"></a>
            <table cellpadding="7">
              <tr>
                <td width="115px">Mission Table</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 1,000,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </div>
          <div class="grid_3 out-gambar">
            <a href=""><img class="gambar" src="<?=base_url();?>assets/perabot/Summit+Mountain+Audio+Rack.jpg"></a>
            <table cellpadding="7">
              <tr>
                <td width="115px">Audio Rack</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 800,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </div>
          <div class="grid_3 out-gambar">
            <a href=""><img class="gambar" src="<?=base_url();?>assets/perabot/Rivera+Sofa.jpg"></a>
            <table cellpadding="7">
              <tr>
                <td width="115px">Rivera Sofa</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 1,000,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </div>
          <div class="grid_3 out-gambar">
            <a href=""><img class="gambar" src="<?=base_url();?>assets/perabot/Puebla+Convertible+Sleeper+Sofa.jpg"></a>
            <table cellpadding="7">
              <tr>
                <td width="115px">Slepper Sofa</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 700,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </div>
          <div class="grid_3 out-gambar">
            <a href=""><img class="gambar" src="<?=base_url();?>assets/perabot/Lounge+Chair.jpg"></a>
            <table cellpadding="7">
              <tr>
                <td width="115px">Lounge Chair</td>
                <td><a class="add" href="">Add to cart</a></td>
              </tr>
              <tr>
                <td>Rp 300,000,-</td>
                <td><a class="add" href="">Wishlist</a></td>
              </tr>
            </table>
          </div>
      </div>
      </div>
    <div class="container_12">
      <div class="grid_12 footer">
        <p>Copyright by Furniture.com Modern Home 2015</p>
      </div>
    </div>
</body>
</html>