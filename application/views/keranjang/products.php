<!DOCTYPE HTML>
<html lang="en-US">
<?php $this->load->view('includes/header'); ?>
<body>
 <div id="products">
 <ul>
  <?php foreach ($products as $product): ?>
  <li>
   <?php echo form_open('keranjang/shop/add'); ?>
   <div class="name"><?php echo $product->nama_barang; ?></div>
 
   <div class="price">Rp <?php echo $product->harga; ?></div>
   <div class="option">
    <?php if ($product->nama_pilihan): ?>
     <?php echo form_label($product->nama_pilihan, 'pilihan_'. $product->id_barang); ?>
     <?php echo form_dropdown(
      $product->nama_pilihan,
      $product->nilai_pilihan,
      NULL,
      'id_barang="option_'. $product->id_barang.'"'
     ); ?>
    <?php endif; ?>
   </div>
   
   <?php echo form_hidden('id_barang', $product->id_barang); ?>
   <?php echo form_submit('action', 'Tambah ke Keranjang'); ?>
   
   <?php echo form_close(); ?>
    
  </li>
  <?php endforeach; ?>
  <a href="shop/show_list"><b>Lihat Keranjang</b></a>
 </ul>
 </div>
 
 
</body>
</html>