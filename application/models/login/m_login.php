<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_login extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->tbl = "member";
	}
 
	function cek_user($username="",$password="")
	{
		$query = $this->db->get_where($this->tbl,array('username' => $username, 'password' => $password));
		$query = $query->result_array();
		return $query;
	}
	function ambil_user($username)
        {
        $query = $this->db->get_where($this->tbl, array('username' => $username));
        $query = $query->result_array();
        if($query){
            return $query[0];
        }
    }
	
	function save() {
		$data = array('username'=>$this->input->post('username'),
		'alamat'=>$this->input->post('alamat'),
		'no_hp'=>$this->input->post('no_hp'),
		'password'=>$this->input->post('password'),
		'level'=>$this->input->post('level')
	);
		$this->db->insert('member',$data);
	}

}
