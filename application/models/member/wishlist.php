<?php
class wishlist extends CI_Model {
private $primary_key='id_barang';
	private $tbl_name='barang';
	
	function __construct()
    {
        parent::__construct();
    }
	
	function get_all() {
  
  $results = $this->db->get('wishlist')->result();
  
  return $results;
 }
	
	public function delete($id_barang) {
		$this->db->delete('wishlist', array('id_barang' => $id_barang));
	}
	
	
	function get($id_barang) {
        $query = $this->db->get_where('wishlist', array('id_barang'=>$id_barang));
        return $query->row();
    }


	
}

