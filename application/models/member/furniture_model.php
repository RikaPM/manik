<?php
class furniture_model extends CI_Model {
private $primary_key='id_barang';
	private $tbl_name='barang';
	
	function __construct()
    {
        parent::__construct();
    }
	
	function get_all($limit = NULL, $offset = NULL) {
        $query = $this->db->get('barang', $limit, $offset);
        return $query->result();
    }
	public function delete($id_barang) {
		$this->db->delete('barang', array('id_barang' => $id_barang));
	}
	
	

	function edit_data_barang($id_barang)//edit
	{
	$q="SELECT * FROM barang WHERE id_barang='$id_barang'";
	$query=$this->db->query($q);
	return $query->row();
	}
	
	function simpan_update_barang($id_barang, $nama_barang, $harga) //simpan dan update
	{
	$data = array(
	'id_barang' => $id_barang,
	'nama_barang' => $nama_barang,
	'harga' => $harga
	
	);
	$this->db->where('id_barang', $id_barang);
	$this->db->update('barang', $data);
	}



	function save() {												//simpan inputan
		$data = array('id_barang'=>$this->input->post('id_barang'),
		'nama_barang'=>$this->input->post('nama_barang'),
		'harga'=>$this->input->post('harga')
		
	);
		$this->db->insert('barang',$data);
	}
	function get($id_barang) {
        $query = $this->db->get_where('barang', array('id_barang'=>$id_barang));
        return $query->row();
    }


	

}
