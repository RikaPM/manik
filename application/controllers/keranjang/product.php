<?php
class Shop extends CI_Controller {
 function __construct()
   {
       parent::__construct();
   }

   function index()
    {
        $this->load->library('template');
      $this->load->model('keranjang/products_model','product',true);
       $data['product_list'] = $this->product->get_all();
        $this->template->display('product', $data);
    }
 }

 /*function index() {
  
  $this->load->model('keranjang/Products_model');
  
  $data['products'] = $this->Products_model->get_all();
  $this->load->view('keranjang/products', $data);
  
  //$data['main_content'] = $this->Products_model->get_all();
  //$this->load->view('includes/template', $data);		
 }
 
 function add() {
  
  $this->load->model('keranjang/Products_model');
  
  $product = $this->Products_model->get($this->input->post('id_barang'));
  
  $insert = array(
   'id_barang' => $this->input->post('id_barang'),
   'qty' => 1,
   'harga' => $product->harga,
   'nama_barang' => $product->nama_barang
  );
  if ($product->nama_pilihan) {
   $insert['options'] = array(
    $product->nama_pilihan=> $product->nilai_pilihan[$this->input->post($product->nama_pilihan)]
   );
  }
  
  $this->cart->insert($insert);
 
  redirect('keranjang/shop');
  
 }
 
 function remove($rowid) {
  
  $this->cart->update(array(
   'rowid' => $rowid,
   'qty' => 0
  ));
  
  redirect('keranjang/shop/show_list');
  
 }
 
 function show_list(){
	$this->load->view('keranjang/list');
 }
 
}*/