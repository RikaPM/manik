<?php if ( ! defined('BASEPATH')) exit('No direct script access
allowed');

 class Cart extends CI_Controller {

   function __construct()
   {
        parent::__construct();
        $this->load->model('keranjang/products_model','product',true);
       $this->load->library('cart');
        $this->load->library('template');
    }

    function add($id_barang) {
        $product = $this->product->get($id_barang);

        $data = array(
            'id_barang'      => $product->id_barang,
           'qty'      => 1,
            'harga'   => $product->harga,
			'jumlah_tersedia' => $product->jumlah_tersedia,
           'nama_barang'   => $product->nama_barang
        );

        $this->cart->insert($data);
       redirect("keranjang/cart");
   }

   function update()
    {
        $this->cart->update($_POST);
       redirect("keranjang/cart");
    }

    function index() {
        $data['cart_list'] = $this->cart->contents();
       $this->template->display('keranjang/cart', $data);
    }

 }
.