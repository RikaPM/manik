<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_user extends CI_Controller {
public function __Construct()
{
parent ::__construct();
$this->load->model('login/m_login');
}

function index()
	{
		$ambil_akun = $this->m_login->ambil_user($this->session->userdata('uname'));
		$data = array(
			'user'	=> $ambil_akun,
			);
		$stat = $this->session->userdata('lvl');
		if($stat==1){//admin
		
		redirect('user/c_user/view_admin');
		}else{ //user
			redirect('user/member/index');
		}
		
	}
	function login()
	{
		$session = $this->session->userdata('isLogin');
    	if($session == FALSE)
    	{
      		$this->load->view('login/form_login');
    	}else
    	{
      		redirect('user/c_user');
    	}
	}

	function logout(){
     $this->session->unset_userdata('isLogin');
     redirect('nonmember/home','refresh');
	}
	
	function view_admin()
	{
	$this->load->model('admin/furniture_model'); 
	$data['data_barang'] = $this->furniture_model->getAll(); 
    $data['username'] = $this->session->userdata('uname');
	$this->load->view('admin/front_admin');
	$this->load->view('admin/side');
	$this->load->view('admin/barang',$data);
	$this->load->view('admin/v_footer');
	}
	
	
	
	
	
	
	public function tambah()//tampilkan view tambah
	{
	$this->load->model('admin/furniture_model'); 
	$data['data_barang'] = $this->furniture_model->getAll(); 
	$this->load->view('admin/front_admin');
	$this->load->view('admin/side');
	$this->load->view('admin/tambahbarang',$data);
	$this->load->view('admin/v_footer');
	}
		
	function insert() { //insert barang ke db
		if ($this->input->post('submit')) {
		$this->load->model('admin/furniture_model'); 
			$this->furniture_model->save();
		}
		redirect('user/c_user/index');
	}
	
	function edit_data_barang($id_barang)//tampilan untuk mengedit
	{
	$this->load->model('admin/furniture_model');
	$data['edit']=$this->furniture_model->edit_data_barang($id_barang);
	$this->load->view('admin/front_admin');
	$this->load->view('admin/side');
	$this->load->view('admin/edit_barang',$data);
	$this->load->view('admin/v_footer');
	}
	
	public function delete($id_barang) 
	{
	$this->load->model('admin/furniture_model');
	$this->furniture_model->delete($id_barang);
	redirect('user/c_user/index');
	}
	
	

	function simpan_update_barang() //update edit data barang ke database
	{$this->load->model('admin/furniture_model');
	$id_barang = $this->input->post('id_barang');
	$nama_barang = $this->input->post('nama_barang');
	$harga = $this->input->post('harga');
	$data['edit'] = $this->furniture_model->simpan_update_barang($id_barang, $nama_barang, $harga);
	$data['konfir_simpan'] = 'Data telah berhasil disimpan';
	$this->load->view('admin/notifikasi_simpan', $data);
	}	
}
	
	
	









