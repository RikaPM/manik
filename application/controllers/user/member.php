<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class member extends CI_Controller {
public function __Construct()
{
parent ::__construct();
$this->load->model('member/furniture_model'); 
}
	  function index()
   {
   $data['username'] = $this->session->userdata('uname');
   $this->load->view('member/home', $data);
   $this->load->view('member/side');
   $this->load->model('member/furniture_model','member',true);
   $data['product_list'] = $this->member->get_all();
   $this->load->view('member/member',$data);
   $this->load->view('member/v_footer');
   }
   
  function index2()
   {
   $data['username'] = $this->session->userdata('uname');
   $this->load->view('member/home', $data);
   $this->load->view('member/side');
   $this->load->model('member/wishlist');
   $data['product_list'] = $this->wishlist->get_all();
   $this->load->view('member/wishlist',$data);
   $this->load->view('member/v_footer');
   }

	public function delete2($id_barang) 
	{
    $this->load->model('member/wishlist');
	$this->wishlist->delete($id_barang);
	redirect('user/member/index');
	}
	
	function add_wish ($id_barang) {
       $product = $this->furniture_model->get($id_barang);
       $data = array(
            'id_barang'      => $product->id_barang,
            'harga'   => $product->harga,
            'nama_barang'    => $product->nama_barang
			
       );

        $this->db->insert('wishlist',$data);
        redirect("user/member/index2");
    }
	 
	
	
	}
	