<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('login/m_login'); //memasukkan file model m_login.php ke dalam controller
    }
    function index()
    {
        $session = $this->session->userdata('isLogin'); //mengambil dari session apakah sudah login atau belum
        if($session == FALSE) //jika session false maka akan menampilkan halaman login
        {
            $this->load->view('login/form_login');
        }else //jika session true maka di redirect ke halaman dashboard
        {
            redirect('user/c_user');
        }
    }
    function do_login()
    {
        $username = $this->input->post("uname");
        $password = $this->input->post("pass");
        
        $cek = $this->m_login->cek_user($username,$password); //melakukan persamaan data dengan database
        if(count($cek) == 1){ //cek data berdasarkan username & pass
            foreach ($cek as $cek) {
                $level = $cek['level']; //mengambil data(level/hak akses) dari database
            }
            $this->session->set_userdata(array(
                'isLogin'   => TRUE, //set data telah login
                'uname'  => $username, //set session username
                'lvl'      => $level, //set session hak akses
            ));
                
            redirect('user/c_user','refresh');  //redirect ke halaman dashboard
        }else{ //jika data tidak ada yng sama dengan database
            echo "<script>alert('Gagal Login!')</script>";
            redirect('login/login','refresh');
        }
        
    }
	
	
	
	
	
		function signup()
	{
	  $data['main_content'] = 'login/register';
	$this->load->view('includes/template', $data);
	}
	function create_member()
	{
		if ($this->input->post('submit')) {
		$this->load->model('login/m_login'); 
			$this->m_login->save();
		}
		redirect('login/login');
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
